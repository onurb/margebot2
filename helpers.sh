#!/bin/bash


source .env

########
# single
########

delete_mr_single(){
    local mr=$1
    curl --silent --request DELETE --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/${mr}"
    echo "Delete !${mr}"
}

# generic
update_mr_single(){
    local attribute=$1
    local value=$2
    local mr=$3
    curl --silent --request PUT --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/${mr}?${attribute}=${value}"
}

close_mr_single(){
    local mr=$1
    update_mr_single "state_event" "close" "${mr}"
    echo "Close !${mr}"
}

unlabel_single(){
    local mr=$1
    update_mr_single "labels" "\"\"" "${mr}"
    echo "Remove all labels of !${mr}"
}

assign_to_bot_single(){
    local mr=$1
    # TODO use update_mr_single
    # update_mr_single "assignee_id" "${BOT_ID}" "${mr}"
    curl --silent --request PUT --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$mr}?assignee_id=${BOT_ID}" &> /dev/null ;
    echo "!${mr} assigned to bot" ;
}

target_default_single(){
    local mr=$1
    local default='bruno-nl-main'
    # TODO use update_mr_single
    # update_mr_single "target_branch" "${default}" "${mr}"
    curl --silent --request PUT --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$mr}?target_branch=${default}" &> /dev/null ;
    echo "!${mr} now targets ${default}"
}

approve_single(){

    local mr=$1
    curl --silent --request POST --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$mr}/approve" &> /dev/null ;
    curl --silent --request POST --header "PRIVATE-TOKEN: ${MARGEBOT2_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$mr}/approve" &> /dev/null ;
    echo "!${mr} approved"
}

unapprove_single(){

    local mr=$1
    curl --silent --request POST --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$mr}/unapprove" &> /dev/null ;
    curl --silent --request POST --header "PRIVATE-TOKEN: ${MARGEBOT2_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$mr}/unapprove" &> /dev/null ;
    echo "!${mr} unapproved"
}

mark_ready_single(){
    local mr=$1
    curl --silent --request POST --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/${mr}/notes?body=%2Fready"
    echo "!${mr} marked as ready"
}

##########
# multiple
##########

close_mr_multiple(){
    for mr in "$@"; do
        close_mr_single "$mr"&
    done
    wait
}

target_default_multiple(){
    for mr in "$@"; do
        target_default_single "$mr"&
    done
    wait
}

assign_to_bot_multiple_par(){
    for mr in "$@"; do
        assign_to_bot_single "$mr"&
    done
    wait
}

assign_to_bot_multiple(){
    for mr in "$@"; do
        assign_to_bot_single "$mr"
    done
}

unlabel_multiple(){
    for mr in "$@"; do
        unlabel_single "$mr"
    done
}

approve_multiple(){
    for mr in "$@"; do
        approve_single "$mr"
    done
}

unapprove_multiple(){
    for mr in "$@"; do
        unapprove_single "$mr"
    done
}

mark_ready_multiple(){
    for mr in "$@"; do
        mark_ready_single "$mr"
    done
}


#######
# range
#######

delete_range(){

    # Check if two arguments are provided
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <start_integer> <end_integer>"
        exit 1
    fi

    local start="$1"
    local end="$2"

    # Test if both arguments are integers
    if ! is_integer "$start"; then
        echo "Error: The first argument is not an integer."
        exit 1
    fi

    if ! is_integer "$end"; then
        echo "Error: The second argument is not an integer."
        exit 1
    fi

    # Print numbers from start to end
    if [ "$start" -le "$end" ]; then
        for ((i = start; i <= end; i++)); do
            delete_mr_single "$i"
        done
    else
        echo "Error: The start integer is greater than the end integer."
        exit 1
    fi
}

assign_to_bot_range(){

    # Check if two arguments are provided
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <start_integer> <end_integer>"
        exit 1
    fi

    local start="$1"
    local end="$2"

    # Test if both arguments are integers
    if ! is_integer "$start"; then
        echo "Error: The first argument is not an integer."
        exit 1
    fi

    if ! is_integer "$end"; then
        echo "Error: The second argument is not an integer."
        exit 1
    fi

    # Print numbers from start to end
    if [ "$start" -le "$end" ]; then
        for ((i = start; i <= end; i++)); do
            assign_to_bot_single "$i"
        done
    else
        echo "Error: The start integer is greater than the end integer."
        exit 1
    fi
}

assign_to_bot_sleep_range(){

    # Check if two arguments are provided
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <start_integer> <end_integer>"
        exit 1
    fi

    local start="$1"
    local end="$2"
    local sleep_duration=20

    # Test if both arguments are integers
    if ! is_integer "$start"; then
        echo "Error: The first argument is not an integer."
        exit 1
    fi

    if ! is_integer "$end"; then
        echo "Error: The second argument is not an integer."
        exit 1
    fi

    # Print numbers from start to end
    if [ "$start" -le "$end" ]; then
        for ((i = start; i <= end; i++)); do
            assign_to_bot_single "$i"
            sleep $sleep_duration
        done
    else
        echo "Error: The start integer is greater than the end integer."
        exit 1
    fi
}

assign_to_bot_range_par(){

    # Check if two arguments are provided
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <start_integer> <end_integer>"
        exit 1
    fi

    local start="$1"
    local end="$2"

    # Test if both arguments are integers
    if ! is_integer "$start"; then
        echo "Error: The first argument is not an integer."
        exit 1
    fi

    if ! is_integer "$end"; then
        echo "Error: The second argument is not an integer."
        exit 1
    fi

    # Print numbers from start to end
    if [ "$start" -le "$end" ]; then
        for ((i = start; i <= end; i++)); do
            assign_to_bot_single "$i"&
        done
        wait
    else
        echo "Error: The start integer is greater than the end integer."
        exit 1
    fi
}

# Function to check if a string is an integer
is_integer() {
    local s="$1"
    [[ "$s" =~ ^-?[0-9]+$ ]]
}

approve_range(){

    # Check if two arguments are provided
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <start_integer> <end_integer>"
        exit 1
    fi

    local start="$1"
    local end="$2"

    # Test if both arguments are integers
    if ! is_integer "$start"; then
        echo "Error: The first argument is not an integer."
        exit 1
    fi

    if ! is_integer "$end"; then
        echo "Error: The second argument is not an integer."
        exit 1
    fi

    # Print numbers from start to end
    if [ "$start" -le "$end" ]; then
        for ((i = start; i <= end; i++)); do
            approve_single "$i"
        done
    else
        echo "Error: The start integer is greater than the end integer."
        exit 1
    fi
}

unlabel_range(){

    # Check if two arguments are provided
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <start_integer> <end_integer>"
        exit 1
    fi

    local start="$1"
    local end="$2"

    # Test if both arguments are integers
    if ! is_integer "$start"; then
        echo "Error: The first argument is not an integer."
        exit 1
    fi

    if ! is_integer "$end"; then
        echo "Error: The second argument is not an integer."
        exit 1
    fi

    # Print numbers from start to end
    if [ "$start" -le "$end" ]; then
        for ((i = start; i <= end; i++)); do
            unlabel_single "$i"
        done
    else
        echo "Error: The start integer is greater than the end integer."
        exit 1
    fi
}


unapprove_range(){

    # Check if two arguments are provided
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <start_integer> <end_integer>"
        exit 1
    fi

    local start="$1"
    local end="$2"

    # Test if both arguments are integers
    if ! is_integer "$start"; then
        echo "Error: The first argument is not an integer."
        exit 1
    fi

    if ! is_integer "$end"; then
        echo "Error: The second argument is not an integer."
        exit 1
    fi

    # Print numbers from start to end
    if [ "$start" -le "$end" ]; then
        for ((i = start; i <= end; i++)); do
            unapprove_single "$i"
        done
    else
        echo "Error: The start integer is greater than the end integer."
        exit 1
    fi
}

mark_ready_range(){

    # Check if two arguments are provided
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <start_integer> <end_integer>"
        exit 1
    fi

    local start="$1"
    local end="$2"

    # Test if both arguments are integers
    if ! is_integer "$start"; then
        echo "Error: The first argument is not an integer."
        exit 1
    fi

    if ! is_integer "$end"; then
        echo "Error: The second argument is not an integer."
        exit 1
    fi

    # Print numbers from start to end
    if [ "$start" -le "$end" ]; then
        for ((i = start; i <= end; i++)); do
            mark_ready_single "$i"
        done
    else
        echo "Error: The start integer is greater than the end integer."
        exit 1
    fi


}

approve_range_par(){

    # Check if two arguments are provided
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <start_integer> <end_integer>"
        exit 1
    fi

    local start="$1"
    local end="$2"

    # Test if both arguments are integers
    if ! is_integer "$start"; then
        echo "Error: The first argument is not an integer."
        exit 1
    fi

    if ! is_integer "$end"; then
        echo "Error: The second argument is not an integer."
        exit 1
    fi

    # Print numbers from start to end
    if [ "$start" -le "$end" ]; then
        for ((i = start; i <= end; i++)); do
            approve_single "$i"&
        done
        wait
    else
        echo "Error: The start integer is greater than the end integer."
        exit 1
    fi
}


#######
# other
#######

# https://docs.gitlab.com/ee/api/discussions.html#list-project-merge-request-discussion-items
# GET /projects/:id/merge_requests/:merge_request_iid/discussions
get_mr_discussions(){
    curl -s --request GET --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$1}/discussions" | jq
}

# https://docs.gitlab.com/ee/api/labels.html#delete-a-label
# DELETE /projects/:id/labels/:label_id
delete_labels(){
    for label in "$@"; do
        curl -s --request DELETE --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}"  "https://gitlab.com/api/v4/projects/${PROJECT_ID}/labels/{$label}" | jq
    done
}

get_info_mr(){
    curl -s --request GET --header "PRIVATE-TOKEN: ${TESTER_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$1}" | jq
}
