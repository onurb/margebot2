# Changelog

## Version 0.3.0

### New Features

- Attempt adding MRs to merge train up to 15 times, waiting 2 seconds between two consecutive attempts, to circumvent GitLab API flakiness.

- Marge messages (when problems are detected in MR or when addition to merge train failed) are updated and are now clearer.

- Sleep time between bot runs is now a CLI argument instead of a hardcoded constant.

## Version 0.2.0

### New Features

- Log `detailed_merge_status`.

- Set `with_merge_status_recheck` parameter to true when querying the list of merge requests to have the latest merge status (cf. gitlab [doc](https://docs.gitlab.com/ee/api/merge_requests.html#merge-requests-list-response-notes)).

## Version 0.1.0

Initial version
