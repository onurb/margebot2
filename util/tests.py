import sys, subprocess, time, os, logging, argparse, gitlab
from marge_bot2.setup import *

manual=False
auto=False

def getenv(name, err_msg):
    val = os.getenv(name)
    if val is None:
        print(f"Please set environment variable '{name}' {err_msg}", file=sys.stderr)
        sys.exit(1)
    else:
        return val

remote = getenv('REMOTE', 'to contain the remote to be used for testing.')
default = getenv('DEFAULT_BRANCH', 'to contain the default branch to be used for testing.')
author = getenv('AUTHOR', 'to contain the GitLab user who should act as author.')
bot = getenv('BOT', 'to contain the GitLab username of the bot.')
# TODO get [bot_id] it from gitlab project?
bot_id = getenv('BOT_ID', 'to contain the GitLab user id of the bot.')
REPO_DIR = getenv('REPO_DIR', 'to contain the git repository to be used for testing.')

logger = logger_setup('TEST')

def run_command(command, cwd=None):
    # It's better to let command be a list and setting shell to False,
    # see https://stackoverflow.com/questions/15109665/subprocess-call-using-string-vs-using-list
    # for more info.
    result = subprocess.run(command, cwd=cwd, shell=(type(command) is str), capture_output=True, text=True)
    logger.info(f"Running '{command}'" + (f" in {cwd}" if cwd else ""))
    if result.returncode != 0:
        raise Exception(f"Command failed: {command}\nOutput: {result.stdout}\nError: {result.stderr}")
    if result.stdout.strip():
        for line in result.stdout.splitlines():
            logger.debug("STDOUT: " + line)
    if result.stderr.strip():
        for line in result.stderr.splitlines():
            logger.debug("STDERR: " + line)
    return result.stdout.strip()


def create_file(path, file_name, content):
    with open(os.path.join(REPO_DIR, f"{path}{file_name}"), 'w') as file:
        file.write(content)


#####################################################################
#
#   GIT OPERATIONS
#
#####################################################################

def create_branch(branch_name):
    run_command(f"git fetch {remote} {default}", cwd=REPO_DIR)
    run_command(f"git checkout -b {branch_name} {remote}/{default}", cwd=REPO_DIR)

def commit_file(path, file_name):
    run_command(f"git add {path}{file_name}", cwd=REPO_DIR)
    run_command(f"git commit --no-verify {path}{file_name} -m 'Add {file_name}'", cwd=REPO_DIR)


# TODO assignee: assignee_id: must be the number and not the name
def push_mr(project, source, target, title, description, labels, assignee, is_draft):
    base_push_command = f"git push {remote} {source}"
    run_command(base_push_command, cwd=REPO_DIR)
    #logger.debug(f"push mr assignee: {assignee}")
    title = "Draft: " + title if is_draft else title
    options = {'source_branch': source,
               'target_branch': target,
               'title': title,
               'description': description,
               'labels': labels,
               'remove_source_branch': True}
    logger.debug(options)
    mr = project.mergerequests.create(options)
    logger.debug(f"MR !{mr.iid}")
    return mr

def git_checkout(branch, force_create=False, start_point = None):
    if force_create:
        run_command(f"git checkout -B {branch}", cwd=REPO_DIR)
    else:
        run_command(f"git checkout {branch}", cwd=REPO_DIR)

def git_push_branch(remote, branch, args=[]):
    command = ["git", "push", remote, branch] + args
    run_command(command, cwd=REPO_DIR)

def git_branch_exists(branch):
    # TODO: using exception handling for control flow here is hacky
    try:
        run_command(["git", "show-ref", "--quiet", f"refs/heads/{branch}"], cwd=REPO_DIR)
        return True
    except Exception as e:
        return False

def delete_branch(source, default):
    git_checkout(default)
    run_command(f"git branch -D {source}", cwd=REPO_DIR)

def push_file_branch(remote, branch, filedescr):
    path, filename, content = filedescr
    git_checkout(branch)
    run_command("git pull", cwd=REPO_DIR)
    create_file(path, filename, content)
    commit_file(path, filename)
    run_command(f"git push {remote} {branch}", cwd=REPO_DIR)


#####################################################################
#
#   MR CREATION
#
#####################################################################

# generic

def create_mr(project, files, source, target, title, description, assignee, is_draft=False, labels=[]):
    create_branch(source)

    for (path, file_name, content) in files:
        create_file(path, file_name, content)
        commit_file(path, file_name)

    mr = push_mr(project, source, target, title, description, labels, assignee, is_draft)
    delete_branch(source, default = target)
    return mr

def create_dummy_mr(project,
                    source_prefix = "dummy-branch",
                    target=default,
                    title="(No title)",
                    description ="(None)",
                    assignee = author,
                    is_draft=False,
                    labels=[],
                    ci=None,
                    ci_timeout="20m"):
    assignee = assignee
    title = title
    timestamp = str(int(time.time()))
    source = f"{source_prefix}-{timestamp}"
    files = [("src/", timestamp, "dummy")]
    if not(ci is None):
        files.append(("", ".gitlab-ci.yml", ci_generic(ci, ci_timeout)))
    return create_mr(project, files, source, target, title, description, assignee, is_draft, labels)

def create_happy_case_mr(project, source_prefix, base_labels):
    return create_dummy_mr(project,
                           source_prefix = source_prefix,
                           title="Happy case",
                           description="Should be merged",
                           is_draft=False,
                           labels= base_labels + ["happy"],
                           ci=None,
                           ci_timeout="2m")

def create_wrong_target_mr(project, source_prefix, base_labels, wrong_target = 'master'):
    return create_dummy_mr(project,
                           source_prefix = source_prefix,
                           target= wrong_target,
                           title="Wrong target branch. MR should be ignored by bot.",
                           description="Should be ignored",
                           is_draft=False,
                           labels= base_labels + ["wrong-target"],
                           ci=None,
                           ci_timeout="2m")

def create_draft_mr(project, source_prefix, base_labels):
    return create_dummy_mr(project,
                           source_prefix = source_prefix,
                           title=" not ready (draft)",
                           description="MR is draft. Should not be merged",
                           is_draft=True,
                           labels= base_labels + ["draft"],
                           ci=None, ci_timeout="2m")

def create_missing_approval_mr(project, source_prefix, base_labels):
    return create_dummy_mr(project,
                           source_prefix = source_prefix,
                           title="Missing approval",
                           description="MR is missing approvals. Should not be merged.",
                           is_draft=False,
                           labels= base_labels + ["not-approved"],
                           ci=None,
                           ci_timeout="2m")

def create_open_threads_mr(project, source_prefix, base_labels):
    mr = create_dummy_mr(project,
                         source_prefix = source_prefix,
                         title="Open threads",
                         description="MR has unresolved discussions. Should not be merged.",
                         is_draft=False,
                         labels= base_labels + ["open-threads"],
                         ci=None,
                         ci_timeout="2m")
    mr.discussion = mr.discussions.create({'body': 'First comment of discussion'})
    return mr

# CI

def ci_generic(script, timeout):
    content = f"""
stages:
  - sleep

sleep:
  stage: sleep
  script:
    - {script}
  timeout: {timeout}
  only:
    - merge_requests
  variables:
    GIT_STRATEGY: none
"""
    return content

ci_fails = "exit 1"

def ci_sleep(s):
    return f"sleep {s}"

def ci_loop(s):
    return f"while true; do echo \"Sleeping for {s} seconds...\"; sleep {s}; done"

def create_mr_failed_pipeline(project, source_prefix="dummy-branch", base_labels=[], title = "Failed pipeline"):
    return create_dummy_mr(project,
                           source_prefix = source_prefix,
                           title = title,
                           description = "Pipeline will fail. Will not be merged by bot.",
                           labels=base_labels + ["ci-fails"],
                           ci = ci_fails)

def create_mr_long_pipeline(project, labels, ci_timeout):
    create_dummy_mr(project, title = "Long pipeline", description = "Pipeline will always be running. Will not be merged by bot.",
                    labels=labels, ci = "while true; do echo \"Sleeping for 10 seconds...\"; sleep 10; done", ci_timeout = ci_timeout)

def create_mr_rebase_conflict(project, source_prefix, base_labels):
    timestamp = str(int(time.time()))
    path = "src/"
    target = default

    # create dummy mr
    file_source = (path, timestamp, "dummy")
    source = f"{source_prefix}-{timestamp}"
    assignee = author
    title = "Rebase conflict"
    description = "Rebase conflicts need to be solved. Will not be merged by bot."
    mr = create_mr(project,
                   [file_source],
                   source,
                   target,
                   title,
                   description,
                   assignee,
                   labels=base_labels + ["rebase-conflict"])

    # push to target branch modification that will create conflict with source branch
    file_target = (path, timestamp, "lock")
    push_file_branch(remote, target, file_target)

    return mr

def create_mr_all_pbs(project, source_prefix, base_labels):
    timestamp = str(int(time.time()))
    path = "src/"
    target = default

    # create dummy mr
    file_source = (path, timestamp, "dummy")
    source = f"{source_prefix}-{timestamp}"
    assignee = author
    title = "Draft: All problems: not ready, missing approval, open threads, rebase conflict."
    description = "Will not be merged by bot."
    mr = create_mr(project,
                   [file_source],
                   source,
                   target,
                   title,
                   description,
                   assignee,
                   labels=base_labels + ["all-pbs", "draft", "not-approved", "open-threads", "rebase-conflict"])

    # push to target branch modification that will create conflict with source branch
    file_target = (path, timestamp, "lock")
    push_file_branch(remote, target, file_target)

    # create open thread
    mr.discussion = mr.discussions.create({'body': 'First comment of discussion'})
    return mr


#####################################################################
#
#   OTHER MR OPERATIONS
#
#####################################################################

def approve(mr):
    try:
        mr.approve()
        logger.debug(f"MR !{mr.iid} <{mr.title}> approved")
    except Exception as e:
        logger.debug(f"Failed to approve MR !{mr.iid}: {e}")
        return

def assign(mr, assignee_id):
    try:
        mr.assignee_id = assignee_id
        mr.save()
        logger.debug(f"MR !{mr.iid} <{mr.title}> assigned to {assignee_id}")
    except Exception as e:
        logger.error(f"Failed to assign !{mr.iid} to {assignee_id}")

def assign_user(mr, assignee):
    assign(mr, assignee.id)

# TODO understand why it does not work
#def merge_request_merge_trains(gl, project, mr: gitlab.v4.objects.MergeRequest):
#def merge_request_merge_trains(gl, project, mr: gitlab.v4.objects.merge_requests.ProjectMergeRequest):
def merge_request_merge_trains(gl, project, mr):
    # type is 'gitlab.v4.objects.merge_requests.ProjectMergeRequest' tested with python-gitlab 4.9.0
    # print(type(mr))
    '''Get merge train for a given merge request.'''
    path = f"{gl.api_url}/projects/{mr.project_id}/merge_trains/merge_requests/{mr.iid}"
    json = gl.http_get(path)
    return gitlab.v4.objects.ProjectMergeTrain(
        # Not sure this is the correct manager
        manager=project.merge_trains,
        attrs=json
    )

#####################################################################
#
#   SCENARIOS
#
#####################################################################


# TODO delete once scenarios completed
def make_demo(project, title=None,labels = ["demo","long"], mrs_nb = 5, is_draft=True, ci_fails=True, ci_loop=True, open_threads=True, rebase_conflict=True, ci_timeout="2m"):
    # 5 MRs that should be merged by bot
    for i in range(mrs_nb-1):
        mr_title = f"Should be merged by bot ({i+1}/{mrs_nb})" if title is None else title
        create_dummy_mr(project, title=mr_title, description = "Should be merged by the bot", labels=labels)
    # Draft
    if is_draft:
        create_dummy_mr(project, title="Draft MR", description = "In draft mode. Will not be merged by bot.", is_draft=True, labels=labels)
    # Pipeline that fails
    if ci_fails:
        create_mr_failed_pipeline(labels)
    # good MR in the middle
    mr_title = f"Should be merged by bot ({mrs_nb}/{mrs_nb})" if title is None else title
    create_dummy_mr(project, title=mr_title, description = "Should be merged by the bot", labels=labels)
    # Pipeline running indefinitely
    if ci_loop:
        create_mr_long_pipeline(project, labels, ci_timeout)
    # Unresolved discussions
    if open_threads:
        create_open_threads_mr(project, labels)
    # Rebase conflict
    if rebase_conflict:
        create_mr_rebase_conflict(project, labels)

def show_tell():
    st_labels = ["show & tell", "script"]
    create_dummy_mr(project, title="show & tell 1/6", description = "Should be merged by the bot", labels=st_labels)
    create_dummy_mr(project, title = "show & tell 2/6 CI FAIL", description = "Pipeline will fail. Will not be merged by bot.", labels=st_labels, ci = "exit 1")
    for i in range(3,7):
        create_dummy_mr(project, title=f"show & tell {i}/6", description = "Should be merged by the bot", labels=st_labels)

def make_custom(project):
    labels = ["demo-cancel-3"]
    create_dummy_mr(project, title="Should be merged 1/2", description="Should be merged", labels=labels)
    create_mr_failed_pipeline(labels)
    create_dummy_mr(project, title="Should be merged 2/2", description="Should be merged", labels=labels)

def make_long_demo(project):
    labels = ["demo-long", "assign-sleep"]
    mrs_nb = 100
    for i in range(mrs_nb):
        mr_title = f"ci: 5m - assign/sleep ({i+1}/{mrs_nb})"
        create_dummy_mr(project, title=mr_title, description = "Should be merged by the bot", labels=labels)

def make_test_vm(project):
    labels = ["test-vm"]
    mrs_nb = 10
    for i in range(mrs_nb):
        mr_title = f"ci: 5m - assign/sleep ({i+1}/{mrs_nb})"
        create_dummy_mr(project, title=mr_title, description = "Should be merged by the bot", labels=labels)

def make_stacked_mrs(project, source_prefix="dummy-branch", base_labels=[], mrs_nb=3):
    target = default
    created_mrs = []
    logger.info(f"Creating {mrs_nb} stacked MRs.")
    for i in range(mrs_nb):
        mr_title = f"stacked MRs ({i+1}/{mrs_nb})"
        mr = create_dummy_mr(project,
                             source_prefix,
                             title=mr_title,
                             target = target,
                             description = "Stacked MRs. Should be merged by the bot",
                             labels = base_labels + ["stacked"])
        target = mr.source_branch
        created_mrs.append(mr)
    return created_mrs

def make_test_mr_analysis(project):
    labels = ["mr-analysis","test"]
    # approval
    create_dummy_mr(project, source_prefix="test-mr-analysis", title="Missing approval", labels=labels)
    # draft
    create_dummy_mr(project, source_prefix="test-mr-analysis", title="Draft error", labels=labels, is_draft=True)
    # loop
    create_dummy_mr(project, source_prefix="test-mr-analysis", title="CI loop", labels=labels, ci=ci_loop(10), ci_timeout="20m")
    # fail
    create_dummy_mr(project, source_prefix="test-mr-analysis", title="CI fail (no note)", labels=labels, ci=ci_fails)
    # open threads
    create_dummy_mr(project, source_prefix="test-mr-analysis", title="Open threads (manual)", labels=labels)
    # rebase conflict
    create_dummy_mr(project, source_prefix="test-mr-analysis", title="Rebase conflicts (manual)", labels=labels)
    # all problems
    create_dummy_mr(project, source_prefix="test-mr-analysis", title="All problems (rebase and threads manual)", is_draft=True, ci=ci_loop(10), ci_timeout="20m", labels=labels)

def make_test_pipeline(project):
    labels = ["mr-38","pipelines"]
    create_dummy_mr(project, source_prefix="test-mr-38", title="CI loop", labels=labels, ci=ci_loop(10), ci_timeout="20m")

def make_test_dependencies(project):
    labels = ["mr-dependencies"]
    source_prefix = "test-mr-dependencies"
    create_dummy_mr(project, source_prefix=source_prefix, title="1/2 MR1 (no dependencies, CI fails)", labels=labels + ["ci-fails"], ci=ci_fails)
    create_dummy_mr(project, source_prefix=source_prefix, title="2/2 MR2 (depends on MR1)", labels=labels)

def make_test_pipeline_running(project, labels):
    labels = ["pipeline-running"] + labels
    source_prefix = "test-pipeline-running"
    create_dummy_mr(project, source_prefix=source_prefix, title="CI pipeline running for 3 min", labels=labels, ci=ci_loop(10), ci_timeout="3m")


# issue #19 https://gitlab.com/nomadic-labs/marge-bot2/-/issues/19
# | ci-running | CI is running | Bot ignores it. MR keep being assigned to bot. MR will be merged once CI is successfully finished. | Tested and to retest after changes to bot implemented|
# | ci-fail | MR with CI that always fails. | Bot will add MR to merge-train and reassign to author. A merge train pipeline will start and fail. MR will not be merged. |Tested and to retest after changes to bot implemented|
# | ci-canceled | Latest CI pipeline is canceled.| Bot adds it to a merge train. Merge train pipeline is created and, if successful, MR will be merged. MR is reassigned to bot. | To be tested. |
# | ci-blocked | Latest CI pipeline is blocked.| Bot adds it to a merge train. Merge train pipeline is created and, if successful, MR will be merged. MR is reassigned to bot. | To be tested. (Needs modification of CI configuration) |
# | ci-no-head | MR with no pipeline on target-branch. (e.g. stacked MRs) | Bot will create a new pipeline and then handle | Implementation in review. Tested for stacked MRs. Need to detect no regression introduced.  cf. https://gitlab.com/nomadic-labs/marge-bot2/-/issues/16 |
# | mr-dependencies-happy | [Dependent MRs](https://docs.gitlab.com/ee/user/project/merge_requests/dependencies.html). MR2 depends on MR1. MR1 is merged. MR2 is ready to merge and assigned to bot. | Bot adds it to merge train, MR is merged and MR reassigned to author| Tested manually (TODO find trace). To retest after bot updates. |
# | mr-dependencies-unblocked | Dependent MRs. MR2 depends on MR1. MR1 is *not* merged and not assigned to bot. MR2 is otherwise ready to merge and assigned to bot. | ~~Bot detects dependency, does not add it to merge train and keep MR assigned to bot~~ (Edit: MR dependencies not accessible [through API](https://docs.gitlab.com/ee/user/project/merge_requests/dependencies.html#api-support-for-managing-merge-request-dependencies)) Bot tries to add it to merge train and fails with an error 400. Note is written in MR and MR reassigned to author. | Tested in https://gitlab.com/nomadic-labs/marge-bot2/-/issues/15. To retest after bot updates. |
# if [analysis_case] is [False], removes all cases related to the MR analysis (draft, threads, conflicts, approvals)
# iterates [iterations] times on the creation of MRs
# each iteration produces [happy_cases] happy case MRs and sequences of [stack] stacked MRs
def make_scenarios(project, analysis_case=True, iterations=1, happy_cases=40, stack=3):
    timestamp = str(int(time.time()))
    base_labels = ["issue-19","scenarios", timestamp]
    source_prefix = "scenarios"
    mrs_to_approve_and_assign = []
    number_of_created_mrs = iterations * (happy_cases + (4 if analysis_case else 0) + 2 + stack)
    logger.info(f"Creating {number_of_created_mrs} MRs")
    for i in range(iterations):
        logger.info(f"Iteration {i+1}/{iterations}.")
        # happy cases
        logger.info(f"Creating {happy_cases} happy case MRs.")
        for i in range(happy_cases):
            happy_mr = create_happy_case_mr(project, source_prefix, base_labels)
            mrs_to_approve_and_assign.append(happy_mr)
        # wrong target
        logger.info("Creating a MR not targeting the default branch")
        wrong_target_mr = create_wrong_target_mr(project, source_prefix, base_labels)
        mrs_to_approve_and_assign.append(wrong_target_mr)
        # draft
        if analysis_case:
            logger.info("Creating a draft MR")
            draft_mr = create_draft_mr(project, source_prefix, base_labels)
            mrs_to_approve_and_assign.append(draft_mr)
        # missing approval
        if analysis_case:
            logger.info("Creating MR with missing approvals")
            not_approved_mr = create_missing_approval_mr(project, source_prefix, base_labels)
            assign(not_approved_mr, bot_id)
        # open threads
        if analysis_case:
            logger.info("Creating MR with unresolved threads")
            open_threads_mr = create_open_threads_mr(project, source_prefix, base_labels)
            mrs_to_approve_and_assign.append(open_threads_mr)
        # rebase conflict
        if analysis_case:
            logger.info("Creating MR with rebase conflicts")
            conflict_mr = create_mr_rebase_conflict(project, source_prefix, base_labels)
            mrs_to_approve_and_assign.append(conflict_mr)
        # # all pbs: draft, missing approvals, open threads, rebase conflict
        # FIX assignment fails - not sure why
        # if analysis_case:
        #     logger.info("Creating MR with all problems that can be detected (draft, threads, conflicts, approvals)")
        #     all_pbs_mr = create_mr_all_pbs(project, source_prefix, base_labels)
        #     assign(all_pbs_mr, bot_id)
        # ci fails
        logger.info("Creating MR with pipeline that will fail")
        ci_fail_mr = create_mr_failed_pipeline(project, source_prefix, base_labels)
        mrs_to_approve_and_assign.append(ci_fail_mr)
        # stacked mrs
        logger.info(f"Creating {stack} stacked MRs")
        stacked_mrs = make_stacked_mrs(project, source_prefix, base_labels, stack)
        mrs_to_approve_and_assign += stacked_mrs

    logger.debug(f"{len(mrs_to_approve_and_assign)} MRs to approve and assign")
    for mr in mrs_to_approve_and_assign:
        approve(mr)
        assign(mr, bot_id)

def make_wrong_target(project, nb=1):
    timestamp = str(int(time.time()))
    base_labels = ["issue-19", "scenarios", "wrong-target", timestamp]
    source_prefix = "scenarios"
    logger.info(f"Creating {nb} MRs not targeting the default branch. (Wrong target)")
    for i in range(nb):
        mr = create_wrong_target_mr(project, source_prefix, base_labels)
        approve(mr)
        assign(mr, bot_id)

def make_happy_cases(project, nb=1):
    timestamp = str(int(time.time()))
    base_labels = ["issue-19","scenarios", "happy-cases", timestamp]
    source_prefix = "scenarios"
    happy_mrs = []
    logger.info(f"Creating {nb} MRs that should be merged by the bot. (Happy cases)")
    for i in range(nb):
        # happy case
        happy_mr = create_happy_case_mr(project, source_prefix, base_labels)
        approve(happy_mr)
        happy_mrs.append(happy_mr)
    for mr in happy_mrs:
        assign(mr, bot_id)
        #return happy_mr

def make_bug_retry_running_pipeline(project, sleep=60, nb=1):
    timestamp = str(int(time.time()))
    base_labels = ["bug","retry", "running-pipeline", timestamp]
    source_prefix = "scenarios"
    logger.info(f"Creating {nb} MR(s) in draft mode to test retries when a pipeline is running.")
    for i in range(nb):
        mr = create_dummy_mr(project,
                             source_prefix,
                             title=f"Bug retry ({i+1}/{nb})",
                             description="Start a pipeline and mark the MR ready. Observe if new attemps whilst pipeline is running",
                             is_draft=True,
                             ci = ci_sleep(sleep),
                             labels=base_labels)
        approve(mr)
        assign(mr, bot_id)


# function that will be called in debug-mode
def scratch(project):
    logger.debug("Entering scratch function")
    test = "scenarios"
    mrs = project.mergerequests.list(iterator=True, labels = [f"{test}"], state='opened', order_by='updated_at', sort='asc')
    logger.debug(f"Number of MRs: {len(mrs)}")
    for mr in mrs:
        logger.debug(f"!{mr.iid} <{mr.title}>")
        try:
            approve(mr)
            assign(mr, bot_id)
            logger.debug(f"Assignees: {mr.assignee}")
            #mr.approve() # will fail if already approved by user
        except Exception as e:
            #logger.debug(f"Failed to approve MR !{mr.iid}: {e}")
            logger.debug(f"Failed to get assignee MR !{mr.iid}: {e}")
            return
        # TODO approve
        # https://docs.gitlab.com/ee/api/merge_request_approvals.html#approve-merge-request
        # POST /projects/:id/merge_requests/:merge_request_iid/approve
        # not clear how to do it with python-gitlab


def main():

    parser = argparse.ArgumentParser(
        prog='tests',
        description='Creates, approves and assigns merge requests.'
    )


    parser.add_argument('author_private_token', type=str, help='A GitLab personal access token for the default MR author.')
    parser.add_argument(
        'project_id',
        type=str,
        help='The GitLab project the bot should process. Can be a numerical project id or a NAMESPACE/PROJECT string.'
    )
    parser.add_argument('default_branch', type=str, help='The default branch of the project.')
    parser.add_argument('margebot2_private_token', type=str, help='A GitLab personal access token for the user the bot should incarnate.')
    parser.add_argument('--debug', action='store_true', help='Run tests in debug mode')
    # Parse arguments
    # Parse arguments
    args = parser.parse_args()

    # Get the arguments from sys.argv
    author_private_token = args.author_private_token
    margebot2_private_token = args.margebot2_private_token
    project_id_or_name_with_namespace = args.project_id
    project_default_branch = args.default_branch
    debug = args.debug

    # initialise gitlab connection
    try:
        gl = gitlab.Gitlab('https://gitlab.com', private_token=author_private_token)

        if os.getenv('MB2_DEBUG', ""):
            logger.info(f"Enabling python-gitlab's debug mode")
            gl.enable_debug()


        # Get the project
        project = gl.projects.get(project_id_or_name_with_namespace)

        # Get current user
        gl.auth()
        bot_user = gl.user

    except Exception as e:
        logger.error(f"Some problem occurred during initialisation {e}.")
        print(help)
        sys.exit(1)

    if debug:
        # scratch(project)
        # mr = project.mergerequests.get(1275)
        mrs_to_approve_and_assign = []
        ci_fail_mr = create_mr_failed_pipeline(project, source_prefix="debug", base_labels=["debug"])
        mrs_to_approve_and_assign.append(ci_fail_mr)
        #mrs_to_approve_and_assign = make_stacked_mrs(project, base_labels = ["test"])
        for mr in mrs_to_approve_and_assign:
            approve(mr)
            assign(mr, bot_id)
        # run_margebot2(margebot2_private_token,
        #                   project_id_or_name_with_namespace,
        #                   project_default_branch)
        return

    try:
        run_command("eval $(ssh-agent) ; ssh-add", cwd=REPO_DIR)
        # manual
        if manual:
            manual_labels = ["show & tell", "manual"]
            create_dummy_mr(project, title="Happy case", description="Should be merged", is_draft=False, labels=manual_labels, ci=None, ci_timeout="2m")
            create_dummy_mr(project, title="Blocked", description="Blocked because draft", is_draft=True, labels=manual_labels, ci=None, ci_timeout="2m")
        # automatic
        if auto:
            #make_demo(project, labels = ["demo", "long"], mrs_nb = 3, is_draft=False, ci_fails=False, ci_loop=False, open_threads=False, rebase_conflict=False)
            #make_demo(project, ci_timeout="20m")
            show_tell()
        if not(auto):
            #make_long_demo(project)
            #make_test_vm(project)
            #make_stacked_mrs(project, 3, ["mr-49"])
            #make_test_mr_analysis(project)
            #make_test_pipeline(project)
            #make_test_dependencies(project)
            #create_mr_rebase_conflict(project, source_prefix = "rebase-conflict", labels = ["rebase-conflict", "test"])
            #make_test_pipeline_running(project, ["mr-49"])
            # create_mr_failed_pipeline(project, source_prefix="test-merge-immediately", labels = ["merge-immediately", "issue-18"], title = "Failed pipeline (try Merge immediately)")
            # create_mr_failed_pipeline(project, source_prefix="test-merge-immediately", labels = ["merge-immediately", "issue-18"], title = "Failed pipeline (try Merge)")
            # create_dummy_mr(project, source_prefix="test-merge-immediately", labels = ["merge-immediately", "issue-18"], title = "Happy case", target="master")
            # create_dummy_mr(project, source_prefix="test-cwd", labels = ["cwd", "debug"], title = "Happy case", target="master")
            #make_scenarios(project, analysis_case=True, iterations=2, happy_cases=15)
            #make_scenarios(project, analysis_case=True, iterations=3, happy_cases=30, stack=3)
            #make_wrong_target(project, 3)
            make_bug_retry_running_pipeline(project, sleep=10, nb=1)
            # make_happy_cases(project, 100)
            # run_margebot2(margebot2_private_token,
            #               project_id_or_name_with_namespace,
            #               project_default_branch,
            #               no_loop=False)

    except Exception as e:
        logger.debug(f"Failed in some test:{e}")
        sys.exit(1)

def run_margebot2_raw(private_token, project_id, default_branch, no_loop=True):
    run_command([
        "python3", "-m", "marge_bot2.bot",
        private_token,
        str(project_id),
        default_branch
    ] + (["--no-loop"] if no_loop else []))

def run_margebot2():
    run_margebot2_raw(margebot2_private_token, project.id, default)


main()
