import sys, os
import json
from datetime import datetime, timezone
import requests, urllib

# Function to print usage information
def print_usage():
    usage_info = """
Usage:
    python gantt.py

    Reads a JSON on standard input corresponding to the output of the
    GitLab list merge trains API and writes a Mermaid Gantt chart
    visualizing the creation and merged date per MR.

    EXAMPLE

    curl https://gitlab.com/api/v4/projects/nomadic-labs%2Fbruno-tezos/merge_trains | \
      python gantt.py | \
      mmdc --input - --output merge_trains.png

    This will read the last 20 merge trains in the GitLab project
    nomadic-labs/bruno-tezos, format as a mermaid diagram, and render it to 'merge_trains.png'
    using mmdc.

    mmdc can be installed through npm using:

    npm install -g @mermaid-js/mermaid-cli

    or rendered by pasting into a HackMD document.

Options:
    --help              Show this help message and exit.
    """
    print(usage_info)

# Check if there's exactly one command-line argument (excluding the script name) and if it's "--help"
if len(sys.argv) == 2 and sys.argv[1] == "--help":
    print_usage()
    exit(1)

# ensure safe does not contain '/'
project = urllib.parse.quote(os.getenv('PROJECT', 'tezos/tezos'), safe='')
margebot_user = os.getenv('MARGEBOT_USER', 'nomadic-margebot')

def get_json(url):
    # Send a GET request to the URL
    response = requests.get(url, headers={'Accept': 'application/json'})

    # Check if the request was successful
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to retrieve {url}: {response.status_code}")
        sys.exit(1)

def merge_trains():
    # The URL from which the JSON data is to be retrieved
    return get_json(f'https://gitlab.com/api/v4/projects/{project}/merge_trains')

def failed_pipelines():
    # The URL from which the JSON data is to be retrieved
    return get_json(f'https://gitlab.com/api/v4/projects/{project}/pipelines/?username={margebot_user}&status=failed&source=merge_request_event')

merge_trains = merge_trains()
failed_pipelines = failed_pipelines()

# Generate Mermaid Gantt chart code
# Reference: https://mermaid.js.org/syntax/gantt.html

# By setting displayMode: compact we can group MRs on the same line
print(f"""---
displayMode: compact
---
gantt
    title Merge trains
    dateFormat  HH:mm:ss
    axisFormat  %H:%M

    %% hide the red line marking the current time
    todayMarker off""")


# Convert timestamps and prepare task lines
task_lines_by_date = {}
for index, item in enumerate(reversed(merge_trains)):
    mr = item['merge_request']

    # Including title is very ugly
    # name = f"!{mr['iid']} ({mr['title']})"
    name = f"!{mr['iid']}"

    created_at_dt = datetime.fromisoformat(item['created_at'].replace('Z', '+00:00'))

    # Set a tag based on the status. The tag influences the color of the bar.
    # 'done' items are grey, 'active' ones blue and 'crit' (critical) is red.
    status = item['status']
    emoji = None
    tag = None
    match status:
        # A merge train is 'fresh' as long as it is running AFAICT.
        case 'fresh':
            tag = 'active'
            emoji = '▶️'
        case 'merged':
            tag = 'done'
            emoji = '✅'
        case 'idle':
            emoji = '😴'
        case 'stale':
            emoji = '🍂'
        case 'skip_merged':
            emoji = '⏭️'
        case _:
            # Unknown merge train status.
            emoji = '⁉️'
            tag = None

    created_at = created_at_dt.strftime('%H:%M:%S')
    if item['duration'] is not None:
        duration_seconds = item['duration']
    else:
        # Get a timezone-aware datetime object for
        # now. [created_at_dt] will be timezone-aware so now must also
        # be so.
        now = datetime.now(timezone.utc)
        time_diff = now - created_at_dt
        duration_seconds = time_diff.seconds

    task_line = f"    {emoji} {name} :"
    if tag is not None:
        task_line = task_line + f'{tag}, '
    task_line = task_line + f"task{index + 1}, {created_at}, {duration_seconds}s\n"

    # section MRs by creation date
    dt = created_at_dt.date()
    if dt in task_lines_by_date:
        task_lines_by_date[dt].append(task_line)
    else:
        task_lines_by_date[dt] = [task_line]

for index, pipeline in enumerate(reversed(failed_pipelines)):
    match pipeline['ref'].split('/'):
        case ["refs", "merge-requests", iid, "train"]:
            mr_iid = iid
        case _:
            # print(f"Ignoring non-merge train pipeline #{pipeline['id']}, ref: {pipeline['ref']}")
            continue

    # name = f"!{mr_iid} (#{pipeline['id']})"
    name = f"!{mr_iid}"
    created_at_dt = datetime.fromisoformat(pipeline['created_at'].replace('Z', '+00:00'))
    updated_at_dt = datetime.fromisoformat(pipeline['updated_at'].replace('Z', '+00:00'))
    emoji = '❌'
    tag = 'done'

    created_at = created_at_dt.strftime('%H:%M:%S')
    time_diff = updated_at_dt - created_at_dt
    duration_seconds = time_diff.seconds

    task_line = f"    {emoji} {name} :"
    task_line = task_line + f'{tag}, '
    task_line = task_line + f"task_fail{index + 1}, {created_at}, {duration_seconds}s\n"

    # section PIPELINEs by creation date
    dt = created_at_dt.date()
    if dt in task_lines_by_date:
        task_lines_by_date[dt].append(task_line)
    else:
        task_lines_by_date[dt] = [task_line]

for dt, task_lines in task_lines_by_date.items():
    print(f"""
    section {dt}""")
    print (''.join(task_lines))
