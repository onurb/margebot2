#!/bin/sh

set -eu
if [ -n "${TRACE:-}" ]; then set -x; fi

# Assumption: tokens.sh containing Gitlab tokens
# TOKEN="glpat-XXX"
source tokens.sh

PROJECT_ID=57223811
BOT_ID=10171167

# assign_mrs_to_bot 42 118 204
assign_mrs_to_bot() {
    for mr_iid in "$@"; do
        curl --request PUT --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$mr_iid}?assignee_id=${BOT_ID}"
    done
}

# assign_mrs_to_bot 51..57
assign_mrs_to_bot_range() {
    eval "assign_mrs_to_bot $(echo {$1})"
}

# https://docs.gitlab.com/ee/api/merge_request_approvals.html#approve-merge-request
# POST /projects/:id/merge_requests/:merge_request_iid/approve

# approve_mrs 42 118 204
approve_mrs() {
    for mr_iid in "$@"; do
        curl --request POST --header "PRIVATE-TOKEN: ${TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/{$mr_iid}/approve"
    done
}

# approve_mrs_range 51..57
approve_mrs_range() {
    eval "approve_mrs $(echo {$1})"
}
