#!/usr/bin/env python3
from marge_bot2 import setup
import sys, gitlab

def is_mr_approved(mr):
    mr_approval_state = mr.approval_state.get().attributes['rules'][0]['approved']
    print(f"Approval state of !{mr.iid}: {mr_approval_state}")
    return mr_approval_state

def approve_even_open_mrs(project, bot_user_id):
    # List all merge requests
    # NB: could not find how to filter MRs lacking approvals directly. Filtering done below.
    # TODO catch exceptions
    merge_requests = project.mergerequests.list(iterator=True, assignee_id=bot_user_id, state='opened')

    for mr in merge_requests:
        if mr.iid % 2 == 0 and (not (is_mr_approved(mr))):
            try:
                # Approve the merge request
                mr.approve()
                # TODO assign to bot
                print(f"Approved MR !{mr.iid}")
            except gitlab.exceptions.GitlabOperationError as e:
                print(f"Failed to approve MR !{mr.iid}: {e}")

def main():
    help = "Usage: approve_mrs.py <MR_author_private_token> <project_id> <bot_id>"
    # Check if the required number of arguments is provided
    if len(sys.argv) != 4:
        print(help)
        sys.exit(1)

    # Get the arguments from sys.argv
    token = sys.argv[1]
    project_id = sys.argv[2]
    bot_user_id = sys.argv[3]

    try:
        project = setup.get_gitlab_project(token, project_id)
        approve_even_open_mrs(project, bot_user_id)
    except Exception as e:
        print(e)
        print(help)

main()
