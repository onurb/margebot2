#!/usr/bin/env python3

import sys, gitlab

# Relevant documentation
# https://docs.gitlab.com/ee/api/projects.html#edit-project
# https://python-gitlab.readthedocs.io/en/stable/api-levels.html

def merge_trains(private_token, project_id, flag_bool):

    # TODO ask for confirmation bf changing settings
    # NOTE : API does not seem to provide ability to check what the status is before changing anything

    gl = gitlab.Gitlab('https://gitlab.com', private_token=private_token)

    path = f"https://gitlab.com/api/v4/projects/{project_id}"
    data = {}
    data["merge_pipelines_enabled"] = flag_bool
    data["merge_trains_enabled"] = flag_bool
    # TODO use a logger or `--default` option
    # print (f"merge_trains: {data}")
    return gl.http_put(path, post_data = data)

def main():

    help = "Usage: merge_trains_toggle [on|off]"

    if len(sys.argv) != 4:
        print(help)
        sys.exit(1)

    # Get the arguments from sys.argv
    private_token = sys.argv[1]
    project_id = sys.argv[2]
    option = sys.argv[3]

    flag = True

    if option == "on":
        flag = True
    elif option == "off":
        flag = False
    else:
        print(help)
        sys.exit(1)

    merge_trains(private_token, project_id, flag)

main()
