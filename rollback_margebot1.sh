#!/bin/bash

set -eu

## source environment vars
SCRIPT_DIR="$(dirname "$(realpath "$0")")"
source "$SCRIPT_DIR/.env"

# step 1 stop margebot2
echo "Step 1: Stop margebot2"

# LATER turn mb2 into pip package and binary and then just 'pkill mb2'

process_name="marge_bot2."
margebot_pids=$(ps a | grep ${process_name} | grep python | awk '{print $1}')

if [ -z "$margebot_pids" ]; then
    echo "No process named '$process_name' found."
else
    for pid in $margebot_pids; do
        echo "kill ${pid}"
        kill "$pid"
        #echo "Showing processes"
        #echo "ps a | grep ${process_name} | grep python"
        #ps a | grep ${process_name} | grep python
        if [ $? -eq 0 ]; then
            echo "Successfully killed PID $pid"
        else
            echo "Failed to kill PID $pid"
        fi
    done
fi



# step 2 disable merge trains
echo "Step 2: Disable merge trains"
cd "${MB2_DIR}"
python3 -m util.merge_trains_toggle "$MARGEBOT2_PRIVATE_TOKEN" "$PROJECT_ID" off
echo "Merge trains disabled"

#exec "${MB2_DIR}/merge_trains_toggle off"

# step 3: unassign all MRs from Margebot + write a note in MR
# NB: similar to deploy_margebot2
echo "Step 3: TODO reassign MRs from margebot to author"
# TODO in python, reuse methods from bot.py (maybe add --unassign option to bot)


# step 4: launch margebot 1

echo "Step 4: Launch margebot 1"
cd "${MB1_DIR}"
docker run --restart=on-failure -e MARGE_AUTH_TOKEN="${MARGEBOT2_PRIVATE_TOKEN}" registry.gitlab.com/nomadic-labs/marge-bot:"$(cat version)" --use-https --gitlab-url='http://gitlab.com'
