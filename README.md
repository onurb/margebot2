# Marge-bot 2

Prototype of a bot leveraging GitLab merge trains for merging MRs in tezos/tezos faster thus improving DX.

## Usage through Docker

Marge-bot 2 is distributed as a Docker image. Images are built on the
main branch of this repository under the name:

```shell
$ docker run --env MARGEBOT2_PRIVATE_TOKEN=glpat-MY_TOKEN \
             --env PROJECT=MY_NAMESPACE/MY_PROJECT \
             --env DEFAULT_BRANCH=main \
             registry.gitlab.com/nomadic-labs/marge-bot2:latest
```

This will start the bot and the scanner script. It will handle MRs
targeting `main` in the project `MY_NAMESPACE/MY_PROJECT`,
authenticated as the user of `glpat-MY_TOKEN`.

Alternatively, put

```
MARGEBOT2_PRIVATE_TOKEN=glpat-MY_TOKEN
PROJECT=MY_NAMESPACE/MY_PROJECT
DEFAULT_BRANCH=main
```

in a file `.env` and pass `--env-file .env`:

```shell
$ docker run --env-file .env registry.gitlab.com/nomadic-labs/marge-bot2:latest
```

## Building Docker images locally

To build it:

```shell
$ make docker
```

To run the image after building:

```shell
$ docker run --env ... marge-bot2
```

See above for instructions on how to configure the bot through
environment variables.

## Docker naming scheme

When built in the CI, images are named
```
registry.gitlab.com/nomadic-labs/marge-bot2:BRANCH_NAME
```
or, when the pipeline runs on `main`:
```
registry.gitlab.com/nomadic-labs/marge-bot2:BRANCH_NAME and
registry.gitlab.com/nomadic-labs/marge-bot2:latest
```

When building locally through `make docker`, the image is named `marge-bot2`

Consequently:

 - to use the version created on `main`: `docker run --env-file .env registry.gitlab.com/nomadic-labs/marge-bot2:latest`.
 - to use the version created by the CI on branch `foobar`: `docker run registry.gitlab.com/nomadic-labs/marge-bot2:foobar`
 - to create and use a local image: `make docker && docker run marge-bot2`

## Debug

To enable debug mode of the underlying python-gitlab, set the
environment variable `MB2_DEBUG` to a non-empty value.
