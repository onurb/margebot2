FROM debian:bookworm-slim

# Install wget and ca-certificates (necessary to download get-poetry.sh) and Python
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --yes --no-install-recommends \
    ca-certificates=20230311 \
    wget=1.21.3-1+b2 \
    python3-dev=3.11.2-1+b1 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Don't run as root
RUN adduser --disabled-password marge_bot2 && \
    # Set the working directory in the container
    mkdir -p /home/marge_bot2/app && \
    chown marge_bot2:marge_bot2 /home/marge_bot2/app && \
    # https://python-poetry.org/docs/#installation
    wget -O get-poetry.py https://install.python-poetry.org && \
    POETRY_HOME=/home/marge_bot2/.poetry python3 get-poetry.py && \
    rm get-poetry.py

USER marge_bot2

# Add Poetry to PATH
ENV PATH="/home/marge_bot2/.poetry/bin:${PATH}"

# Install dependencies
WORKDIR /home/marge_bot2/app
COPY poetry.lock pyproject.toml ./
RUN poetry install

# Copy the rest of your application into the Docker image
COPY --chown=marge_bot2:marge_bot2 ./ /home/marge_bot2/app
# Create dummy .env file -- configuration is done through the
# environment variables:
# - MARGEBOT2_PRIVATE_TOKEN
# - PROJECT
# - DEFAULT_BRANCH
RUN touch .env
# Create a folder for log files
# TODO: I don't think logs should be written inside the container.
RUN mkdir /home/marge_bot2/app/marge_bot2/logs

# Run marge-bot
ENTRYPOINT ["poetry", "run", "./margebot2"]
