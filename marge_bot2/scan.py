from marge_bot2.setup import *
import sys, time

def log_merge_train_item(logger, item):
    attributes = item.attributes
    merge_train_log = f"Merge train {attributes['id']} ({attributes['status']})"
    pipeline = attributes['pipeline']
    if (pipeline is None):
        pipeline_log = "no pipeline"
    else:
        pipeline_log = f"pipeline #{pipeline['id']} ({pipeline['status']})"
    mr = attributes['merge_request']
    mr_log = f"!{mr['iid']} [{mr['title']}] ({mr['state']})"
    logger.info(f"{merge_train_log}: {pipeline_log}, MR {mr_log}")

def log_merge_train(logger, project):
    try:
        active_merge_trains = project.merge_trains.list(iterator=True, scope="active")
    except Exception as e:
        logger.error(f"Cannot find list of merge trains: {e}.")
        return
    logger.info(f"Begin scan of active merge trains.")
    for item in active_merge_trains:
        try:
            log_merge_train_item(logger, item)
        except Exception as e:
            logger.error(f"Some problem occurred whilst scanning an active merge train: {e}.")
            logger.error("Details of active merge train below.")
            logger.error(item)
            return
    logger.info(f"{len(active_merge_trains)} MRs to be merged.")

def main():

    help = "Usage: scan.py <private_token> <project_id>"
    # Check if the required number of arguments is provided
    if len(sys.argv) != 3:
        print(help)
        sys.exit(1)

    # Get the arguments from sys.argv
    private_token = sys.argv[1]
    project_id_or_name_with_namespace = sys.argv[2]

    logger = logger_setup('SCAN')

    try:
        # TODO get user as well as in bot.py to log for debug purposes?
        project = get_gitlab_project(private_token, project_id_or_name_with_namespace)
    except Exception as e:
        logger.error(f"Could not access project {project_id_or_name_with_namespace} with given token: {e}.")
        return
    logger.info(f"Scanner for project {project_id_or_name_with_namespace} successfully initialised.")

    sleep_duration = 2

    while True:
        log_merge_train(logger, project)
        logger.info(f"End of scan. Sleeping for {sleep_duration} seconds.")
        time.sleep(sleep_duration)

main()
