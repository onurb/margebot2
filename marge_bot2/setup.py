# Logging setup
import logging, datetime, os

def logger_setup(name):
    logger = logging.getLogger(name)
    format_string = '[%(name)s] %(asctime)s %(levelname)s %(message)s'
    logging_level = logging.DEBUG # TODO set-up with a cli option?
    # Determine the base directory
    base_dir = os.path.dirname(os.path.abspath(__file__))
    log_dir = f'{base_dir}/logs'
    # Creates log_dir if needed. Does not fail if already exists.
    os.makedirs(log_dir, exist_ok=True)
    log_filename = f'{log_dir}/{datetime.datetime.now().strftime("%Y%m%d%H%M%S")}-{name}.log'

    logging.basicConfig(format=format_string, filename=log_filename, encoding='utf-8', level=logging_level)

    # create console handler to print log content in stdout
    ch = logging.StreamHandler()
    ch.setLevel(logging_level)
    formatter = logging.Formatter(format_string)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


# Gitlab setup
import gitlab

def get_gitlab_project(private_token, project_id_or_name_with_namespace):
    gl = gitlab.Gitlab('https://gitlab.com', private_token=private_token)
    return gl.projects.get(project_id_or_name_with_namespace)
