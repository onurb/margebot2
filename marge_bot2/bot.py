from marge_bot2.setup import *
import sys, time, argparse


## add MR to merge train on python-gitlab
# not available in python-gitlab but PR exists https://github.com/python-gitlab/python-gitlab/pull/2552/files
def add_mr_to_merge_train_raw(gl, mr, when_pipeline_succeeds = None, squash = None):
    # Build and send http request
    path = f"{gl.api_url}/projects/{mr.project_id}/merge_trains/merge_requests/{mr.iid}"
    data = {}
    if when_pipeline_succeeds:
        data["when_pipeline_succeeds"] = when_pipeline_succeeds
    if squash:
        data["squash"] = squash

    gl.http_post(path, post_data=data)


# assumes mr.iid and mr.title exist. check done in bot()
# assumes gl.api_url exists. check done in main()
# Values of [retries] and [retry_sleep] must be chosen so that the worst case scenario is acceptable.
def add_mr_to_merge_train(logger, gl, project, mr, retries, retry_sleep):
    '''As add_mr_to_merge_train_raw but catches exceptions and posts them as notes on the MR.

    Additionally, if [retries] is a positive integer, the failures
    are ignored and the submission is re-attempted after [retry_sleep]
    seconds, up to [retries] times.'''

    when_pipeline_succeeds = False
    squash = False
    logger.debug(f"Adding !{mr.iid} <{mr.title}> to merge train with 'when_pipeline_succeeds={when_pipeline_succeeds}', 'squash={squash}'")

    attempts = 0
    # NB: the loop will not stop if the MR is merged by other means or becomes unmergeable (closed, draft, open threads, etc.)
    # In our experiments, state of the MR is not reliable, which prevents us to use it in the loop condition.
    while True:
        attempts += 1
        try:
            add_mr_to_merge_train_raw(gl, mr, when_pipeline_succeeds, squash)
            logger.info(f"!{mr.iid} [{mr.title}] added to a merge train. (Attempt {attempts}/{retries+1})")
            break
        except Exception as e:
            if attempts > retries:
                logger.error(f"Could not add !{mr.iid} to a merge train. (Attempt {attempts}/{retries+1}) Internal Gitlab Error: {e}")
                logger.debug(f"Detailed merge status of !{mr.iid}: {mr.detailed_merge_status}.")
                note_content = """I can't assign this MR to a merge train!

**Please double-check the merge status of the MR!** (*)

 - **⛔ Merge blocked**: resolve all issues and re-assign to me.

 - **✅ Ready to merge!**: retry in a few seconds, the problem may be transient.

Please contact the infrastructure team on Slack if you need help.

(*) cf. section near top of the MR page, between approvals and activity."""
                add_note(logger, mr, note_content)
                break
            else:
                logger.info(f"Could not add !{mr.iid} to a merge train. Attempt {attempts}/{retries+1}. Retrying in {retry_sleep}s. Internal Gitlab Error: {e}.")
                # Simulates curl --header "PRIVATE-TOKEN: ${MARGEBOT2_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/${MR}"
                logger.debug(f"Dummy call to 'project.mergerequests.get({mr.iid})'. Should improve chances of successful addition to merge train in next attempt.")
                logger.debug(f"Detailed merge status of !{mr.iid}: {mr.detailed_merge_status} (before dummy call).")
                _full_mr = project.mergerequests.get(mr.iid)
                logger.debug(f"Detailed merge status of !{mr.iid}: {mr.detailed_merge_status} (after dummy call).")
                time.sleep(retry_sleep)

# assumes mr.iid and mr.title exist. check done in bot()
# returns None if either:
# - head pipeline or head pipeline status cannot be found
# - head pipeline is null
def get_mr_head_pipeline_status(logger, mr):
    try:
        head_pipeline = mr.head_pipeline
    except Exception as e:
        logger.error(f"No head pipeline found for MR !{mr.iid} <{mr.title}>")
        return None
    if head_pipeline is None:
        return None
    try:
        status = mr.head_pipeline['status']
        logger.debug(f"head pipeline for !{mr.iid} <{mr.title}> has status '{status}'")
        return status
    except Exception as e:
        logger.error(f"Could not check head pipeline status of !{mr.iid} <{mr.title}>: {e}")
        return None

# assumes mr.iid and mr.title exist. check done in bot()
def analyse_mr_errors(logger, mr):
    logger.debug(f"Analyse MR !{mr.iid} <{mr.title}>")
    errors = []
    try:
        if mr.draft:
            errors.append("MR is Draft")
        if not mr.blocking_discussions_resolved:
            errors.append("MR has unresolved discussions")
        if mr.has_conflicts:
            errors.append("MR has rebase conflicts")
        mr_approval_rules = mr.approval_state.get().rules
        if not(all({rule['approved'] for rule in mr_approval_rules})):
            errors.append("MR is missing approvals")
    except Exception as e:
        logger.error(f"Analysis of !{mr.iid} failed: {e}")
    return errors

def add_error_note(logger, mr, errors):
    # begin
    note_content = """I can't assign this MR to a merge train!

Here are the error(s) I have detected:
"""
    # errors
    note_content += "\n".join([f" - {e}" for e in errors])
    # conclusion
    note_content += """

Note that there are some errors I can't detect. So **please have a look at the merge checks done by Gitlab.** (*)

If you need help, please contact the infrastructure team on Slack.

(*) Near top of the MR page, between approvals and activity.
    """
    add_note(logger, mr, note_content)

# assumes mr.iid exists. check done in bot()
def add_note(logger, mr, note_content):
    try:
        mr.notes.create({'body': note_content})
    except Exception as e:
        logger.error(f"Could not create note for !{mr.iid}: {e}")
        return
    logger.debug(f"Note created for !{mr.iid}")
    logger.debug(f"Content of the note \n{note_content}")

# assumes mr.iid exists. check done in bot()
def assign_mr_back_to_author(logger, mr):
    try:
        mr.assignee_id = mr.author.get('id')
        mr.save()
    except Exception as e:
        logger.error(f"Failed to add !{mr.iid} to author.")
    try:
        logger.debug(f"!{mr.iid} assigned to author only ({mr.author.get('name')} @{mr.author.get('username')}).")
    except Exception as e:
        logger.debug(f"!{mr.iid} assigned to author only.")
        logger.error(f"Could not get author's name or username. {e}")
        logger.error(f"MR author data: {mr.author}")

# assumes project.get_id() exists. check done in main()
# assumes bot_user.name and bot_user.id exist. check done in main()
# assumes gl.api_url exists. check done in main()
# if [no_analysis] then no MR checks are done before trying to add to a merge train
def bot(logger, project, default_branch, gl, bot_user, no_analysis, retries, retry_sleep):

    # Get opened merge requests targeting the default branch and assigned to the current user (bot)
    try:
        # uses list MR request https://docs.gitlab.com/ee/api/merge_requests.html#list-merge-requests
        # NB: we have removed the optional argument "with_merge_status_recheck='true'" in the call below
        # to avoid the MR becoming unmergeable during an async call to the merge status
        # cf. https://gitlab.com/gitlab-org/gitlab/-/issues/473389#note_2050877093
        assigned_mrs = project.mergerequests.list(iterator=True, assignee_id=bot_user.id, state='opened', target_branch=default_branch, order_by='updated_at', sort='asc')
    except Exception as e:
        logger.error(f"Could not retrieve list of open MRs assigned to {bot_user.name} (id:{bot_user.id}) and targeting {default_branch}: {e}")
        return

    logger.info(f"{len(assigned_mrs)} open MRs targeting {default_branch} assigned to {bot_user.name}.")

    for mr in assigned_mrs:
        try:
            # uses single MR request https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr
            mr = project.mergerequests.get(mr.iid)
        except Exception as e:
            logger.error(f"Failed to get full MR !{mr.iid} <{mr.title}>: {e} (single MR request)")
            continue
        try:
            # fails if mr.iid does not exist
            logger.info(f"Processing !{mr.iid} <{mr.title}>")
            logger.debug(f"Detailed merge status of !{mr.iid}: {mr.detailed_merge_status}")
        except Exception as e:
            logger.error(f"Failed to get MR iid or title or detailed merge status: {e}")
            logger.error(f"MR data: {mr}")
            continue



        mr_head_pipeline_status = get_mr_head_pipeline_status(logger, mr)
        # if no head pipeline exists, create one and skip the MR
        # MR remains assigned to the bot
        # typical example: stacked MRs
        if mr_head_pipeline_status is None:
            try:
                mr.pipelines.create()
                logger.info(f"Creating pipeline for !{mr.iid} <{mr.title}>.")
                # with the pipeline started, let's ignore this MR until the next iteration
                continue
            except Exception as e:
                logger.error(f"Failed to create pipeline for !{mr.iid} <{mr.title}>.")
                add_error_note(logger, mr, ["Couldn't find a head_pipeline, and couldn't create one"])
                assign_mr_back_to_author(logger, mr)
                continue
        # if head pipeline is running, bot skips the MR, which remains assigned to it
        elif mr_head_pipeline_status == "running":
            logger.debug(f"CI pipeline is running for !{mr.iid} <{mr.title}>. Not adding to merge train. Bot is skipping this MR and will come back to it at next iteration.")
            continue



        mr_errors = analyse_mr_errors(logger, mr) if (not no_analysis) else []
        if mr_errors:
            add_error_note(logger, mr, mr_errors)
        else:
            if no_analysis:
                logger.debug(f"No analysis of !{mr.iid} <{mr.title}> is done before trying to add to merge train.")
            else:
                logger.debug(f"Analysis found no errors in !{mr.iid} <{mr.title}>")
            add_mr_to_merge_train(logger, gl, project, mr, retries, retry_sleep)
        # TODO assign only if something to do?
        # is it in the TODO list https://gitlab.com/dashboard/todos
        # simple solution: add a pass to de-assign author after merged by bot
        # more complex solution:
        # - retrieve pipeline from add to merge train
        # - wait for pipeline result and only assign back to author if not merged
        # other possibility:
        # check at every iteration of the bot or concurrently the MRs merged by margebot and assigned them to her
        assign_mr_back_to_author(logger, mr)


def main():
    parser = argparse.ArgumentParser(
        prog='marge-bot2',
        description='Adds merge requests assigned to it and targetting the DEFAULT_BRANCH to the merge train.'
    )

    parser.add_argument('private_token', type=str, help='A GitLab personal access token for the user the bot should incarnate.')
    parser.add_argument(
        'project_id',
        type=str,
        help='The GitLab project the bot should process. Can be a numerical project id or a NAMESPACE/PROJECT string.'
    )
    parser.add_argument('default_branch', type=str, help='The default branch of the project.')

    parser.add_argument('--no-loop', action='store_true', help='Run marge-bot as a single CLI command, not a service.')

    parser.add_argument('--no-analysis', action='store_true', help='Marge-bot does not check if MR is mergeable (draft, threads, rebase conflicts, approvals).')

    parser.add_argument('--retries', type=int, default=14, help='Number of re-attempts to add a MR to a merge train.')

    parser.add_argument('--retry-sleep', type=int, default=2, help='Seconds of wait between two consecutive attempts to add a MR to a merge train.')

    parser.add_argument('--bot-sleep', type=int, default=5, help='Seconds of wait between two consecutive runs of the bot.')

    # Parse arguments
    args = parser.parse_args()

    # Get the arguments from sys.argv
    private_token = args.private_token
    project_id_or_path_with_namespace = args.project_id
    project_default_branch = args.default_branch
    no_loop = args.no_loop
    no_analysis = args.no_analysis
    retries=args.retries
    retry_sleep=args.retry_sleep
    bot_sleep=args.bot_sleep

    logger = logger_setup('BOT')

    try:
        gl = gitlab.Gitlab('https://gitlab.com', private_token=private_token)

        if os.getenv('MB2_DEBUG', ""):
            logger.info(f"Enabling python-gitlab's debug mode")
            gl.enable_debug()


        # Get the project
        project = gl.projects.get(project_id_or_path_with_namespace)

        # Get current user
        gl.auth()
        bot_user = gl.user

        project_id = project.get_id()
        project_path = project.path_with_namespace

        logger.info(f"Bot {bot_user.name} (id:{bot_user.id}) succesfully initialised, running on project '{project_path}' (#{project_id}). (API url: {gl.api_url})")
        logger.info(f"Bot parameters: {retries} retries, {retry_sleep} seconds between attempts, {bot_sleep} seconds between bot runs.")

        merge_settings = {
            "merge_pipelines_enabled": project.merge_pipelines_enabled,
            "merge_trains_enabled": project.merge_trains_enabled,
            "only_allow_merge_if_pipeline_succeeds": project.only_allow_merge_if_pipeline_succeeds,
        }
        logger.info(f"Project settings: {merge_settings}")

    except Exception as e:
        logger.error(f"Initialisation of bot failed: {e}. Shutting down bot...")
        print(help)
        sys.exit(1)

    while True:
        logger.info(f"{bot_user.name} starts running...")
        bot(logger, project, project_default_branch, gl, bot_user, no_analysis, retries, retry_sleep)
        # Run only one iteration if no_loop is set.
        if no_loop:
            break
        logger.info(f"{bot_user.name} sleeping for {bot_sleep} seconds...")
        time.sleep(bot_sleep)


if __name__ == '__main__':
    main()
