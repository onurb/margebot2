This folder contains unit and integration tests for marge-bot2.

# Running

Tests are executed through poetry:

```
poetry run pytest test/
```
