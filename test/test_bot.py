import logging
from unittest.mock import Mock, call

import gitlab.v4.objects

from marge_bot2 import bot

MOCK_API_URL = 'https://gitlab.example.com/api/v4'
MOCK_PROJECT_ID = 9999

logger = logging.getLogger('TEST')

failed_attempt_note = "I can't assign this MR to a merge train!\n\n**Please double-check the merge status of the MR!** (*)\n\n - **⛔ Merge blocked**: resolve all issues and re-assign to me.\n\n - **✅ Ready to merge!**: retry in a few seconds, the problem may be transient.\n\nPlease contact the infrastructure team on Slack if you need help.\n\n(*) cf. section near top of the MR page, between approvals and activity."

def get_merge_request(gl):
    return gitlab.v4.objects.ProjectMergeRequest(
        manager=gitlab.base.RESTManager(gl, parent = None),
        attrs={
            'iid': 12345,
            'title': "My merge request",
            'project_id': MOCK_PROJECT_ID
        }
    )

def test_add_mr_to_merge_train_no_retry_happy_case():
    '''Test the happy case for add_mr_to_merge_train.

    Test that add_mr_to_merge_train posts to the 'add merge request to merge train' REST API endpoint.
    '''
    gl = Mock()
    gl.api_url = MOCK_API_URL

    project = Mock()

    mr = get_merge_request(gl)
    bot.add_mr_to_merge_train(logger, gl, project, mr, retries = 0, retry_sleep = 0)

    # check that gl.http_post was called exactly once at the given URL
    # and with the given post_data.
    gl.http_post.assert_called_once_with(
        f'{MOCK_API_URL}/projects/9999/merge_trains/merge_requests/12345',
        post_data = {}
    )


def test_add_mr_to_merge_train_no_retry_fail():
    '''Test add_mr_to_merge_train when gl's http_post throws an exception.

    Ensure that a note is posted on the MR.'''

    gl = Mock()
    gl.api_url = MOCK_API_URL
    gl.http_post = Mock(side_effect=[
        # Raised on first call to 'http_post': when attempting to add the MR to the train
        Exception('Dummy exception thrown when attempting to add the MR to the train.'),
        # Returned on second call: when the note is added to the MR
        None
    ])

    project = Mock()

    mr = get_merge_request(gl)
    mr.detailed_merge_status='mergeable'
    bot.add_mr_to_merge_train(logger, gl, project, mr, retries = 0, retry_sleep = 0)

    # check that gl.http_post was called exactly twice at the given URL:
    #  - first to attempt an addition to the merge train. this will fail, as per the side_effect of [http_post]
    #  - second, to post a note on the merge request in question to inform the user that the addition was not
    #    successful.
    assert gl.http_post.call_count == 2

    gl.http_post.assert_has_calls(
        [
            call(f'{MOCK_API_URL}/projects/9999/merge_trains/merge_requests/12345', post_data = {}),
            call('/projects/9999/merge_requests/12345/notes', post_data={'body': failed_attempt_note}, files={})
        ]
    )

def test_add_mr_to_merge_train_retry_success():
    '''Test that add_mr_to_merge_train retries when gl's http_post throws an exception if so configured.

    The first attempt to add the merge request to the train will fail,
    the second will succeed.'''

    gl = Mock()
    gl.api_url = MOCK_API_URL
    gl.http_post = Mock(side_effect=[
        # Raised on first call to 'http_post': when attempting to add the MR to the train
        Exception('Dummy exception thrown when attempting to add the MR to the train.'),
        # Returned on second call: when attempting to add the MR to the train the second time.
        None
    ])

    project = Mock()

    mr = get_merge_request(gl)
    mr.detailed_merge_status='mergeable'
    bot.add_mr_to_merge_train(logger, gl, project, mr, retries = 1, retry_sleep = 0)

    # check that gl.http_post was called to add the MR to train exactly twice.
    assert gl.http_post.call_count == 2
    gl.http_post.assert_has_calls(
        [
            call(f'{MOCK_API_URL}/projects/9999/merge_trains/merge_requests/12345', post_data = {}),
            call(f'{MOCK_API_URL}/projects/9999/merge_trains/merge_requests/12345', post_data = {})
        ]
    )

def test_add_mr_to_merge_train_retry_fail():
    '''Test that add_mr_to_merge_train retries when gl's http_post throws an exception if so configured.

    The first attempt to add the merge request to the train will fail,
    the second will fail again. A note is added to the MR.'''

    gl = Mock()
    gl.api_url = MOCK_API_URL
    gl.http_post = Mock(side_effect=[
        # Raised when attempting to add the MR to the train first time
        Exception('Dummy exception thrown when attempting to add the MR to the train.'),
        # Raised when attempting to add the MR to the train second time
        Exception('Dummy exception thrown when attempting to add the MR to the train.'),
        # Returned when the note is added to the MR
        None
    ])

    project = Mock()

    mr = get_merge_request(gl)
    mr.detailed_merge_status='mergeable'
    bot.add_mr_to_merge_train(logger, gl, project, mr, retries = 1, retry_sleep = 0)

    # check that gl.http_post was called to add the MR to train exactly twice.
    assert gl.http_post.call_count == 3
    gl.http_post.assert_has_calls(
        [
            call(f'{MOCK_API_URL}/projects/9999/merge_trains/merge_requests/12345', post_data = {}),
            call(f'{MOCK_API_URL}/projects/9999/merge_trains/merge_requests/12345', post_data = {}),
            call('/projects/9999/merge_requests/12345/notes', post_data={'body': failed_attempt_note}, files={})
        ]
    )
