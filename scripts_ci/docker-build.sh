#!/bin/sh

set -eu

if [ -n "${TRACE:-}" ]; then set -x; fi

image_name="${CI_REGISTRY}/${CI_PROJECT_PATH}"

# Sanitize branch name to valid docker tag
BRANCH_TAG=$(echo "${CI_COMMIT_REF_NAME}" | tr -c "a-zA-Z0-9_\n" "_")
# Login to the registry, build images and push
docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
# In all branches, we add two tags: the short commit hash and the
# branch name. On the default branch, we also add the
# latest tag.
# shellcheck disable=SC2046
docker build \
    --build-arg=BUILDKIT_INLINE_CACHE=1 \
    --cache-from="${image_name}:latest" \
    --cache-from="${image_name}:${BRANCH_TAG}" \
    -t "${image_name}:${CI_COMMIT_SHORT_SHA}" \
    -t "${image_name}:${BRANCH_TAG}" \
    $(if [ "${CI_COMMIT_BRANCH:-}" = "$CI_DEFAULT_BRANCH" ]; then echo "-t ${image_name}:latest"; fi) \
    .

docker push --all-tags "${image_name}"
