#!/bin/bash

set -eu

## source environment vars
SCRIPT_DIR="$(dirname "$(realpath "$0")")"
source "$SCRIPT_DIR/.env"

# step 1: stop margebot 1
echo "Step  1: Stop margebot 1"

margebot_pid=$(ps a | grep "docker" | grep "MARGE" | awk '{print $1}')

if [ -z "$margebot_pid" ]; then
    echo "No process named found for marge-bot."
else
    kill -KILL "$margebot_pid"
    if [ $? -eq 0 ]; then
            echo "Successfully killed PID $margebot_pid"
        else
            echo "Failed to kill PID $margebot_pid"
        fi
fi

# cleaner way
# docker stop margebot1
# caveat 1: takes longer (and is less reliable) than sending KILL signal which can be a problem for current experiments
# caveat 2: docker container need to be named margebot1 (use --name NAME at *first* run of docker)


# step 2: enable merge trains
echo "Step 2: Enable merge trains"
cd "${MB2_DIR}"
python3 -m util.merge_trains_toggle "$MARGEBOT2_PRIVATE_TOKEN" "$PROJECT_ID" on
echo "Merge trains enabled"

# step 3: unassign all MRs from Margebot + write a note in MR
# NB: similar to rollback_margebot1
echo "Step 3: TODO reassign MRs from margebot to author"
# TODO in python, reuse methods from bot.py (maybe add --unassign option to bot)

# step 4: launch margebot2
echo "Step 4: Launch margebot 2"
./margebot2
